import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
          phone: "",
          checkbox1: false,
          checkbox2: false,
          response: ""
        };
        this.submitForm = this
            .submitForm
            .bind(this);
    }
    async sendPhone(phone) { 
      try {
        let response = await fetch(`/api/sms-promotion?phone=${phone}`);
        let responseJson = await response.json();
        this.setState({response : responseJson});
        
      } catch(error) {
        this.setState({response : error});
      }
  }
  submitForm = (e) =>{
    e.preventDefault();
    if(this.state.checkbox1 && this.state.checkbox2)
      this.sendPhone(this.state.phone);
    else
      this.setState({response:"Please check all boxes"});
  }
  handleInputChange = (e) => {
    const target = e.target;
    const value = target.value;
    const name = target.name;
    this.setState({[name]: value});
  }
  toggleChange1 = () => {
    this.setState({
      checkbox1: !this.state.checkbox1,
    });
  }
  toggleChange2 = () => {
    this.setState({
      checkbox2: !this.state.checkbox2,
    });
  }
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome</h2>
        </div>
        <div className="App-intro">
          <form onSubmit={this.submitForm}>
            <input type="text" name="phone" value={this.state.phone} onChange={this.handleInputChange}/>
            <input type="checkbox" name="checkbox1" checked={this.state.checkbox1} onChange={this.toggleChange1}/><span>I am over 18</span>
            <input type="checkbox" name="checkbox2" checked={this.state.checkbox2} onChange={this.toggleChange2} /><span>I accept the terms and conditions</span>
            <input type="submit" value="submit"/>
          </form>
          <p>{this.state.response}</p>
        </div>
      </div>
    );
  }
}

export default App;
