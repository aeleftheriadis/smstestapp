'use strict';

const smsService = require('../services/smsService');

exports.get_phone = function(req, res) {

    const phone = req.query.phone;
    if(!phone){
        res.status(503).send({error: 'Missing required parameter `phone`'});
        return;
    }
    else{
        const currentDate = new Date();
        let message = 'Good morning! Your promocode is AM123';
        if (currentDate.getHours()>=12)
            message = 'Hello! Your promocode is PM456';     
            
        smsService.send_message(phone,message)
        .then((result)=> res.json(result))
        .catch((result)=> res.status(503).send({error: result}));
    }
};