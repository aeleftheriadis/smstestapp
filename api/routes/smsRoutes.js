'use strict';
module.exports = function(app) {
    const sms = require('../controllers/smsController');

    app.route('/api/sms-promotion/')
        .get(sms.get_phone);

    app.use(function(req, res) {
        res.status(404).send({url: req.originalUrl + ' not found'});
    });
};
