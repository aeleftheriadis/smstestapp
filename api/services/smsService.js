'use strict';

const twilio = require('twilio');

const accountSid = 'AC8c2b4eaefb3f1d659920f522d2979c57'; // Your Account SID from www.twilio.com/console
const authToken = 'af72b4405f8e7b99b10ec7df1cb85673';   // Your Auth Token from www.twilio.com/console

const client = require('twilio')(accountSid, authToken);


exports.validate_phone = function(phone){
    return client.lookups.v1.phoneNumbers(phone)
    .fetch({type: 'carrier'});

};  

exports.send_message = function(phone, message) {
    console.log(phone);
    return client.messages.create({
        body: message,
        to: `+${phone}`,  // Text this number
        from: '+13343103014' // From a valid Twilio number
    })
    .then((message) => {console.log(message.sid); return "Successfully Send"; })
    .catch((error) => { console.log(error.message); return error.message; });
};